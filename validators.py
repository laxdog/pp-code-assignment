import logging
from helpers import colourise_words_in_string
from helpers import colourise_indices_in_string
from helpers import get_indices_of_char

VALIDATORS = {}


class ValidationError(Exception):
    """
    Custom exception class, message will normally be the validor that failed and error will be a colourised string
    of where the error occured in the given string
    """
    def __init__(self, message, error):

        # Call base constructor to get the full object
        super().__init__(message)
        self.message = message
        self.error = error


def add_validator(func):
    """ Used as a decorator for building the list of validators """
    global VALIDATORS
    VALIDATORS[func.__name__] = func


def check_zero_len(string):
    """ Make sure there's something in the input string """
    if len(string) == 0:
        raise ValidationError('Input string was empty', '')


# We need these two log methods as we're not using a class. So if we init the logger at the module level, it gets
# created before the one in the main loop and would miss all it's settings.
def log(message):
    logger = logging.getLogger(__name__)
    logger.info(message)


def debug(message):
    logger = logging.getLogger(__name__)
    logger.debug(message)


@add_validator
def char_is_cap(string, index=0):
    """ Returns true if the chararcter at `index` in `string` is capitalised """
    debug(f'STRING: {string} : Validating if index {index} is capitalised')
    check_zero_len(string)
    if string[index].isupper():
        return True
    else:
        message = f"Character at index {index} was not capitalised"
        error = colourise_indices_in_string(string, [index])
        raise ValidationError(message, error)


@add_validator
def char_is_fullstop(string, index=-1):
    """ Returns true if the chararcter at `index` in `string` is a fullstop """
    debug(f'STRING: {string} : Validating if index {index} is a fullstop')
    check_zero_len(string)
    if string[index] == '.':
        return True
    else:
        message = f"Character at index {index} was not a fullstop"
        error = colourise_indices_in_string(string, [index])
        raise ValidationError(message, error)


@add_validator
def char_count_is_even(string, char='"'):
    """ Returns True if there are an even number of `char` in `string` """
    debug(f'STRING: {string} : Validating if char {char} has an even count')
    check_zero_len(string)
    char_count = string.count(char)
    if char_count % 2 == 0:
        return True
    else:
        indices = get_indices_of_char(string, char)
        # Bug found from hypothesis testing. Breaks the string representation as it tries to print the \n
        if char == '\n':
            char = '<NEWLINE>'
        message = f"Character '{char}' has been used an odd number ({char_count}) times"
        error = colourise_indices_in_string(string, indices)
        raise ValidationError(message, error)


@add_validator
def char_is_unique(string, char='.'):
    """ Returns True if there is only one instance of `char` in `string` """
    debug(f'STRING: {string} : Validating if char {char} is unique or non-existant')
    check_zero_len(string)
    if string.count(char) <= 1:
        return True
    else:
        indices = get_indices_of_char(string, char)
        message = f"Character '{char}' is not unique"
        error = colourise_indices_in_string(string, indices)
        raise ValidationError(message, error)


@add_validator
def string_has_only_longhand_numbers(string, threshold=13):
    """ Returns True if `string` contains no instances of non-spelled out numbers below `threshold` """
    debug(f'STRING: {string} : Validating numbers below {threshold} are spelled out')
    check_zero_len(string)
    # Use list comprehension to get a list of all 'digits' less than 'threshold'
    digit_list = [x for x in string.split() if x.isdigit() and int(x) < threshold]
    if len(digit_list) == 0:
        return True
    else:
        message = f"Numbers below {threshold} should be spelled out"
        error = colourise_words_in_string(string, digit_list)
        raise ValidationError(message, error)
