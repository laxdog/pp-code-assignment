RED = "\u001b[31m"
RESET = "\u001b[0m"


def colourise_indices_in_string(string, indices=[]):
    """ Colours each char red at the given `indices` """
    colored = ""
    # -1 won't be a return value from enumerate, so use the last index
    if -1 in indices:
        indices.append(len(string) - 1)
    for index, char in enumerate(string):
        if index in indices:
            colored += f"{RED}{char}{RESET}"
        else:
            colored += char

    return colored


def colourise_words_in_string(string, words=[]):
    """ Colours each instance of all give `words` red """
    colored = []
    for token in string.split():
        if token in words:
            colored.extend([RED, token, RESET])
        else:
            colored.append(token)

    return " ".join(colored)


def get_indices_of_char(string, search_char):
    """ Find and return each index for all instances of given `search_char` """
    return [index for index, char in enumerate(string) if char == search_char]
