Proofpoint coding assignment
============================

Notes
-----

|               |                                                                                                    |
|---------------|----------------------------------------------------------------------------------------------------|
|Python version | This code must be run on python version 3.6 or above                                               |
|Python modules | Designed to work with only the standard library                                                    |
|Style guide    | This code follows PEP-8 with the exception of line length, which has been increased to 120 char    |
|Note           | Assumes " followed by capital is invalid                                                           |
|Note           | Assumes " after a fullstop is invalid                                                              |

Requirements
------------

* Python script to validate strings against the following conditions
* String starts with a capital letter
* String has an even number of quotation marks
* String ends with a period character "."
* String has no period characters other than the last character
* Numbers below 13 are spelled out ("one", "two", "three", etc)

Installation
------------
There is not currently an installation proccess for the script. Setup files may be added at a later date.

The main script `validate.py` should be called with the python interpretter on command line

```
python3 validate.py
```

Options
-------
```
> python3 validate.py -h
usage: validate.py [-h] [-q] [-v] (-f FILEPATH | -s TEST_STRING)

A string validation program

optional arguments:
  -h, --help            show this help message and exit
  -q, --quiet           Disable all output (check exit codes for verification)
  -v, --verbose         Increase log messages
  -f FILEPATH, --file FILEPATH
  -s TEST_STRING, --string TEST_STRING

```

Running the script
------------------
The script has two main modes of testing, reading from stdin and reading from a file. These are both accessed by
command line switches:

```
python3 validate.py -s "Some string to \"validate\" here."
python3 validate.py -f file.txt
```

Understanding the output
------------------------
There are several things to note:
* Errors shown in stdout will be highlighted in red
* No output will be shown in normal mode if there are no errors. You can turn on verbose to check something is happening.
* You can check the exit code of the program to see how many errors were found during a run
* Quiet mode will not output anything, you must check the exit code. This can be useful for batching.


Running the tests
-----------------
To run the tests you must first install the pytest and hypothesis libraries

```
> pip3 install hypothesis pytest
```

Then run the tests from the root directory

```
> pytest -v
================================================= test session starts =================================================
platform darwin -- Python 3.6.5, pytest-3.6.0, py-1.5.3, pluggy-0.6.0 -- /usr/local/opt/python/bin/python3.6
cachedir: .pytest_cache
hypothesis profile 'default' -> database=DirectoryBasedExampleDatabase('/Users/mrobinson/source/PP-code/.hypothesis/examples')
rootdir: /Users/mrobinson/source/PP-code, inifile:
plugins: hypothesis-3.82.1
collected 8 items

test_validators.py::test_char_is_cap_first_is_upper PASSED                                                      [ 12%]
test_validators.py::test_char_is_cap_first_is_not_upper PASSED                                                  [ 25%]
test_validators.py::test_char_is_fullstop PASSED                                                                [ 37%]
test_validators.py::test_char_is_not_fullstop PASSED                                                            [ 50%]
test_validators.py::test_char_count_is_even PASSED                                                              [ 62%]
test_validators.py::test_char_count_is_odd PASSED                                                               [ 75%]
test_validators.py::test_valid_strings PASSED                                                                   [ 87%]
test_validators.py::test_invalid_strings PASSED                                                                 [100%]

============================================== 8 passed in 2.06 seconds ===============================================
```

Contributing
------------
Please feel free to raise any PRs against this project and I will review them.

Using in your own project
-------------------------
All the validators have been moved to the validators module for ease of use. To include this in your own project
import the VALIDATORS variable. This will give you a list of functions that can be called on a string. This module
has a dependency on the helpers module, so you'll need both.

The helpers module contains the colouring functions and some functions for retrieving indices / words from strings.
This is standalone and can be included in any project without dependencies.

Things that could be added / be improved
----------------------------------------
* Unit tests for the helpers library
* REST API
* Installation files
* Validation could be wrapped in a class. There could be loads of classes used to be honest, but it's not really
    required
* It could fix the string / suggest what it should be
