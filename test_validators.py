from validators import VALIDATORS, ValidationError
import string
import random
import pytest
from hypothesis.strategies import text, characters
from hypothesis import given


random_all = text(alphabet=characters(blacklist_categories=('Cs', )), min_size=1, average_size=None, max_size=None)
lower_text = text(alphabet=characters(blacklist_categories=('Cs', 'Lu')), min_size=1, average_size=None, max_size=None)
no_punc = text(alphabet=characters(blacklist_categories=('Cs', 'Po')), min_size=1, average_size=None, max_size=None)

valid_strings = ['The quick brown fox said "hello Mr lazy dog".',
                 'The quick brown fox said hello Mr lazy dog.',
                 'One lazy dog is too few, 13 is too many.',
                 'One lazy dog is too few, thirteen is too many.']

invalid_strings = ['The quick brown fox said "hello Mr. lazy dog".',
                   'the quick brown fox said "hello Mr lazy dog".',
                   '"The quick brown fox said "hello Mr lazy dog."',
                   'One lazy dog is too few, 12 is too many.']


@given(random_all)
def test_char_is_cap_first_is_upper(random_all):
    assert(VALIDATORS['char_is_cap'](random.choice(string.ascii_uppercase) + random_all))
    for valid_string in valid_strings:
        assert(VALIDATORS['char_is_cap'](valid_string))


@given(lower_text)
def test_char_is_cap_first_is_not_upper(lower_text):
    with pytest.raises(ValidationError) as ex:
        VALIDATORS['char_is_cap'](lower_text)

    assert 'capitalised' in str(ex)


@given(random_all)
def test_char_is_fullstop(random_all):
    assert(VALIDATORS['char_is_fullstop'](random_all + '.'))
    for valid_string in valid_strings:
        assert(VALIDATORS['char_is_fullstop'](valid_string))


@given(no_punc)
def test_char_is_not_fullstop(no_punc):
    with pytest.raises(ValidationError) as ex:
        assert(VALIDATORS['char_is_fullstop'](no_punc))

    assert 'fullstop' in str(ex)


@given(random_all)
def test_char_count_is_even(random_all):
    # Double the input to make sure we've a multiple of 2 of all chars
    char = random.choice(random_all)
    assert(VALIDATORS['char_count_is_even'](random_all * 2, char=char))
    for valid_string in valid_strings:
        assert(VALIDATORS['char_count_is_even'](valid_string))


@given(random_all)
def test_char_count_is_odd(random_all):
    with pytest.raises(ValidationError) as ex:
        # Double the list, then remove a random char to make sure we've an odd number
        double_rand = random_all * 2
        r = random.randrange(len(double_rand))
        char = double_rand[r]
        assert(VALIDATORS['char_count_is_even'](double_rand[:r] + double_rand[r + 1:], char=char))

    assert 'odd number' in str(ex)


def test_valid_strings():
    for func_name, validator in VALIDATORS.items():
        for valid_string in valid_strings:
            assert validator(valid_string)


def test_invalid_strings():
    for invalid_string in invalid_strings:
        with pytest.raises(ValidationError) as ex:
            for func_name, validator in VALIDATORS.items():
                validator(invalid_string)

        assert ex
