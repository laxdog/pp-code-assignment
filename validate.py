import argparse
import logging
import sys
from validators import VALIDATORS, ValidationError


parser = argparse.ArgumentParser(description="A string validation program")
parser.add_argument('-q', '--quiet', action="store_true", default=False,
                    help='Disable all output (check exit codes for verification)')
parser.add_argument('-v', '--verbose', action="store_true", default=False,
                    help='Increase log messages')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-f', '--file', action="store", dest="filepath", type=open)
group.add_argument('-s', '--string', action="store", dest="test_string")

try:
    opts = parser.parse_args()
except FileNotFoundError:
    logging.fatal('File does not exist, please check your command line arguments')
    sys.exit(255)

if opts.quiet:
    logging.basicConfig(stream=sys.stdout, level=logging.CRITICAL)
elif opts.verbose:
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
else:
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

logger = logging.getLogger(__name__)
ERROR_COUNT = 0


def validate_string(string):
    for func_name, validator in VALIDATORS.items():
        try:
            validator(string)
        except(ValidationError) as ex:
            logger.info(f"{ex.message} : {ex.error}")
            global ERROR_COUNT
            ERROR_COUNT += 1


def validate_from_file(filepath):
    # filepath is 'open'ed in the parser, so no need to here
    with filepath as textfile:
        for line in textfile:
            validate_string(line.rstrip('\n'))


if __name__ == "__main__":
    # validate_test_strings()
    if opts.filepath is not None:
        validate_from_file(opts.filepath)
    if opts.test_string is not None:
        validate_string(opts.test_string)
    sys.exit(ERROR_COUNT)
